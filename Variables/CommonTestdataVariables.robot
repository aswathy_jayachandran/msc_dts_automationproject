*** Variables ***

${EnvUrl}    https://dts-dev.msccruises.com
${BrowserSelected}    chrome

#xpath for form click
${formxpath}    /html/body/div/div/div/form

#formFields
${username}    //*[@id="username"]
${password}    password
${office}      office
${loginbutton}    /html/body/div/div/div/form/div[4]/button

#credentials for login
${loginCredentialsusername}    ibsuser2
${loginCredentialpassword}     usr123!
${loginCredentialoffice}        ESP

*** Settings ***
Library    SeleniumLibrary  
Resource    ../Variables/CommonTestdataVariables.robot  

*** Variables ***


*** Keywords ***
Login MSC site
    Open Browser    ${EnvUrl}    ${BrowserSelected}  
    #Open Browser     https://dts27.msccruises.com/     chrome  
    Set Selenium Implicit Wait   10
    Wait Until Page Contains Element     xpath=(${formxpath})  timeout=200
    Click Element              xpath=(${formxpath})
    Wait Until Page Contains Element     xpath=${username}  timeout=100
    Input Text    xpath=${username}        ${loginCredentialsusername}
    Input Text    name=${password}          ${loginCredentialpassword}
    Input Text    name=${office}           ${loginCredentialoffice}
    Click Button    xpath=${loginbutton}